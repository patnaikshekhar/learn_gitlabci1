const express = require('express')

const app = express()

app.get("/", function(req, res) {
    res.end('Hello World')
})

app.listen(8080, () => console.log('Server listening'))